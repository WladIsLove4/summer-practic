import pandas as pd
import psycopg2
from psycopg2 import sql
from contextlib import closing

if __name__ == "__main__":
    df = pd.read_excel('./dataset.xlsx', index_col=0)

    result_values = list(df[:][:].itertuples(index=False, name=None))

    with closing(psycopg2.connect(dbname='Music_project', user='postgres', password="dsdctgbljhs",
                                  host='localhost')) as conn:
        with conn.cursor() as cursor:
            conn.autocommit = True
            print("Создание запроса.")
            insert = sql.SQL(
                'INSERT INTO t_source (user_id, track, artist, genre, city, Time, report_date, weekday) VALUES {}'
            ).format(
                sql.SQL(',').join(map(sql.Literal, result_values))
            )
            cursor.execute(insert)
