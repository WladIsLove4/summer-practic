CREATE TABLE
IF NOT EXISTS t_performances
(
    track_id INT NOT NULL,
    artist_id INT NOT NULL,
    FOREIGN KEY (track_id) REFERENCES t_tracks(track_id) ON DELETE CASCADE,
    FOREIGN KEY (artist_id) REFERENCES t_artists(artist_id) ON DELETE CASCADE,
    PRIMARY KEY(track_id, artist_id)
);
