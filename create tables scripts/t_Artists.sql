
CREATE TABLE
IF NOT EXISTS t_artists
(
    artist_id SERIAL PRIMARY KEY,
    artist_name VARCHAR(255) NOT NULL
);
