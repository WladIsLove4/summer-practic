
CREATE TABLE
IF NOT EXISTS t_cities
(
    city_id SERIAL PRIMARY KEY,
    city_name VARCHAR(50) NOT NULL
);
