-- таблица t_clear_source таблица с очищенными данными

CREATE TABLE
IF NOT EXISTS t_clear_source
(
    ID BIGSERIAL NOT NULL PRIMARY KEY,
    user_id VARCHAR(255),
    track VARCHAR(255),
    artist VARCHAR(255),
    genre VARCHAR(255),
    city VARCHAR(50),
    Time VARCHAR(50),
    report_date DATE,
    weekday VARCHAR(50)
);
