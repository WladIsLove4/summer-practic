
CREATE TABLE 
IF NOT EXISTS t_tracks
(
    track_id SERIAL PRIMARY KEY,
    track_name VARCHAR(255) NOT NULL,
    genre_id INT NOT NULL,
    FOREIGN KEY (genre_id) REFERENCES t_genre (genre_id) ON DELETE SET NULL
);