CREATE TYPE weekday AS ENUM
('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday');

CREATE TABLE
IF NOT EXISTS t_Days
(
    Day_ID SERIAL PRIMARY KEY,
    Day weekday NOT NULL
);
