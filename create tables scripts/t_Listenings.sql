CREATE TABLE
IF NOT EXISTS t_listenings
(
    ID SERIAL PRIMARY KEY,
    user_id BIGINT NOT NULL,
    track_id INT NOT NULL,
    day_id INT NOT NULL,
    city_id INT NOT NULL,
    time Time NOT NULL,
    Report_date DATE NOT NULL,
    FOREIGN KEY(user_id) REFERENCES t_users(user_id) ON DELETE CASCADE,
    FOREIGN KEY(track_id) REFERENCES t_tracks(track_id) ON DELETE CASCADE,
    FOREIGN KEY(day_id) REFERENCES t_days(day_id) ON DELETE CASCADE,
    FOREIGN KEY(city_id) REFERENCES t_cities(city_id) ON DELETE CASCADE
);
